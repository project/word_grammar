Word Grammar
============


The Word Grammar module provides a service that gives you the indefinite article for a given word and a nicely formatted list of words from an array with a parameter for including an Oxford comma (although, really, this should always be true, right?).

This service, particularly the indefinite article generator, is based on English.

Example usage
-------------

```
$wordGrammar = \Drupal::getContainer()->get('word_grammar_service');
$wordGrammar->getIndefiniteArticle('apple'); // returns 'an'
$wordGrammar->getIndefiniteArticle('hour'); // returns 'an'
$wordGrammar->getIndefiniteArticle('horse'); // returns 'a'
$wordGrammar->getIndefiniteArticle('unique'); // returns 'a'

$wordGrammar->getWordList(['horse', 'dog', 'cat', 'chicken', 'elephant'], TRUE); // returns 'horse, dog, cat, chicken, and elephant'
```
